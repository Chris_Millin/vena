//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================
//general items
#include "G_General.h"
//Player
#include "G_Player.h"
//bullet manager
#include "G_BulletManager.h"
//menu
#include "G_Menu.h"

//BG
#include "b_Petri.h"
#include "b_PetriTiles.h"
//=================================

//Variables
//=================================
MapAtt PetriBG;
MapAtt PetriBG0;
MapAtt PetriBG1;

float screenScroll1;
float screenScroll2;

bool Paused;
bool PauseHeld;

bool CompleteMessageShown;
//=================================



void G_Initalise();

void G_Update();

void G_Draw();

void ShowMenu();