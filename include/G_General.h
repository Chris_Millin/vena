#ifndef	G_GENERAL_H
#define G_GENERAL_H


//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================

//=================================

//Variables
//=================================
//stores an array of all the OAM sprites
SpriteAtt OAM_Buffer[128];

u16 OBJ_Offset;

u32 Frame_Count;

bool GameComplete;
bool GameFail;
//=================================

#endif