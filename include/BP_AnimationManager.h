////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Black Paw Animation manager
//
// summary:	simple animation hander
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BP_ANIMATIONMANAGER_H
#define BP_ANIMATIONMANAGER_H

#include "BP_ToolSet.h"
#include "BP_ToolFunct.h"
#include "BP_NoCash.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Animation Structure. </summary>
//
// <remarks>	Chris, 09/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct AnimationStruct
{
	unsigned short startAdd;		//where the animation should start in the array
	unsigned short TileCount;		//how many tiles should be loaded each frame
	unsigned short Frames;			//the amount of frame
	unsigned short FrameDuration;	//the duration of each frame
}AnimationStruct;

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Will run a defined animation by copying the data into the OAM (meaning the sprite
// 				ID will not need to be changed)
// 				The animationTime should be unique to each animation object
// 				This time should be set back to the main time when the animation
// 				has completed (bool returned). </summary>
//
// <remarks>	Chris, 07/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

bool BP_RunAnimation(unsigned int* Src, unsigned int AnimationTime, unsigned short* OBJAddress, AnimationStruct* Animation);

#endif


//for getting the frame of animation
//current time - start time = time elapsed
//now there are 2 ways i believe to do this 
//time elapsed/frames = current frame 
//or
//
//int i = 0;
//int count = 0;
//for(i = 0; count >= timeelaped; i++)
//			count+= frame duration
//i = current frame
//
//both have there pros and con
//The loop could be just as slow as the division
//more important which one gets the right result
//I can sort time out at a later date when I get everything up and running a bit
//better than the current state.
//
//