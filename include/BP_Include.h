#ifndef BP_INCLUDE_H
#define BP_INCLUDE_H



//Other Lib Includes
//=================================
#include <maxmod.h>		// maxmod library
#include "soundbank.h"		// created by building project
#include "soundbank_bin.h"	// created by building project
//=================================

//Black Paw Includes
//=================================
#include "BP_ToolSet.h"
#include "BP_ToolFunct.h"
#include "BP_Graphics.h"
#include "BP_Irq.h"
#include "BP_Input.h"
#include "BP_NoCash.h"
#include "BP_Writer.h"
#include "BP_Collision.h"
#include "BP_Font.h"
#include "BP_LogoOut.h"
#include "BP_AnimationManager.h"
//=================================

//External Includes
//=================================

//=================================

#endif