#ifndef G_MENU_H
#define G_MENU_H

//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================
#include "G_General.h"
//BG
#include "s_Menu.h"
#include "b_Menu.h"
#include "b_MenuTiles.h"
//=================================

typedef enum state
{
	M_START = 0,
	M_ISTART,
	M_INST,
	M_IINST,
	M_ABOUT,
	M_IABOUT
}state;

//Variables
//=================================#
SpriteAtt* mSprites[8];

MapAtt MenuMap;

u32 CursorFlicker;

u8 CurrentState;

u8 MainSelection;

bool KeyDown;
//=================================



void Menu_Initalise();

bool Menu_Update();

#endif