//Chris Millin GBA Batch Converter


#ifndef S_HEALTH_H
#define S_HEALTH_H

extern const short int H1Shape;
extern const short int H1Size;
extern const short int H1Tiles;


extern const short int H2Shape;
extern const short int H2Size;
extern const short int H2Tiles;


extern const short int H3Shape;
extern const short int H3Size;
extern const short int H3Tiles;


extern const short int H4Shape;
extern const short int H4Size;
extern const short int H4Tiles;


extern const short int H5Shape;
extern const short int H5Size;
extern const short int H5Tiles;


extern const short int H6Shape;
extern const short int H6Size;
extern const short int H6Tiles;


extern const short int H7Shape;
extern const short int H7Size;
extern const short int H7Tiles;


extern const short int H8Shape;
extern const short int H8Size;
extern const short int H8Tiles;


extern const short int H9Shape;
extern const short int H9Size;
extern const short int H9Tiles;


extern const short int H10Shape;
extern const short int H10Size;
extern const short int H10Tiles;


extern const short int H11Shape;
extern const short int H11Size;
extern const short int H11Tiles;


extern const short int H12Shape;
extern const short int H12Size;
extern const short int H12Tiles;


extern const short int H13Shape;
extern const short int H13Size;
extern const short int H13Tiles;


extern const short int H14Shape;
extern const short int H14Size;
extern const short int H14Tiles;


extern const short int H15Shape;
extern const short int H15Size;
extern const short int H15Tiles;


extern const short int H16Shape;
extern const short int H16Size;
extern const short int H16Tiles;




extern const short int HealthPallet[];

//const short int H1Pallet;
//const short int H2Pallet;
//const short int H3Pallet;
//const short int H4Pallet;
//const short int H5Pallet;
//const short int H6Pallet;
//const short int H7Pallet;
//const short int H8Pallet;
//const short int H9Pallet;
//const short int H10Pallet;
//const short int H11Pallet;
//const short int H12Pallet;
//const short int H13Pallet;
//const short int H14Pallet;
//const short int H15Pallet;
//const short int H16Pallet;

extern const unsigned int H1[];


extern const unsigned int H2[];


extern const unsigned int H3[];


extern const unsigned int H4[];


extern const unsigned int H5[];


extern const unsigned int H6[];


extern const unsigned int H7[];


extern const unsigned int H8[];


extern const unsigned int H9[];


extern const unsigned int H10[];


extern const unsigned int H11[];


extern const unsigned int H12[];


extern const unsigned int H13[];


extern const unsigned int H14[];


extern const unsigned int H15[];


extern const unsigned int H16[];

#endif
