//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================
#include "G_General.h"
#include "G_BulletManager.h"
#include "G_Player.h"

#include "S_VENASprites.h"
#include <stdlib.h>
//=================================

#define __Enemy__ "[Enemy]"
#define MAX_ENEMIES 10
#define MIN_ENEMIES 1

//Player Structure
//=================================
typedef struct EnemyFormat
{
	Vector2 Position;
	s16 fakeY;
	SpriteAtt* Sprite;
	s16 Health;
	u8 Evolution;
	u32 CreationTime;
	u32 AnimationTime;
	u32 Damaged;
	u8 Type;
	u8 Died;
	bool PreventNew;
	bool Active;
	bool Zeroed;
}EnemyFormat;
//=================================


//Variables
//=================================
EnemyFormat Enemies[MAX_ENEMIES];

Ccube CollisionBox; //since there is going to be just one enemy one collision box is needed

AnimationStruct EnemyAnimation;
AnimationStruct EnemyExplosionAnimation;

u16 OBJEnemyOffset;

u8 KillCount;
//=================================


void Enemies_Initalise();

void Enemies_Update();

void Enemies_GenNew(u8 ID);

//has all the settings for when a AI should evolve
void Enemies_Evolve(u8 ID);

//Enemy version type update
//==================================================================================
void Enemies_T1Update(u8 ID);
void Enemies_T2Update(u8 ID);
void Enemies_T3Update(u8 ID);
void Enemies_T4Update(u8 ID);
void Enemies_T5Update(u8 ID);
//==================================================================================

////////////////////////////////////////////////////////////////////////////////////////////////////
//Notes:
//
// Type 1: moves down the screen
// Type 2: moves top left to bottom right
// Type 3: moves top right to bottom left
// Type 4: moves down stops then moves left
// Type 5: moves down stops then moves right
////////////////////////////////////////////////////////////////////////////////////////////////////

