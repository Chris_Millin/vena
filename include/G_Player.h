
#ifndef G_PLAYER_H
#define G_PLAYER_H

//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================
#include "G_General.h"
#include "G_BulletManager.h"
#include "G_PickUp.h"

#include "S_VENASprites.h"
#include "s_Health.h"
//=================================

#define __PLAYER__ "[Player]"

//Player Structure
//=================================
typedef struct PlayerFormat
{
	Vector2 Position;
	SpriteAtt* Sprite;
	s16 Health;
	u8 Evolution;
	Ccube CollisionBox;
	u32 AnimationTime;
	u8 BulletDamage;
}PlayerFormat;
//=================================


//Variables
//=================================
PlayerFormat Player;

AnimationStruct PlayerAnimation;
AnimationStruct PlayerDeathAnimation;

u16 OBJPlayerOffset;

SpriteAtt* HealthSprite;

u32 ShootTimer;

bool PlayerEnabled;
//=================================


void Player_Initalise(u16 PosX, u16 PosY);

void Player_Update();

void UpdateHealth();

void Player_DisbaleHealthBar();

#endif
