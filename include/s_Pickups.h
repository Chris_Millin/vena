//Chris Millin GBA Batch Converter


#ifndef S_PICKUPS_H
#define S_PICKUPS_H

extern const short int PH1Shape;
extern const short int PH1Size;
extern const short int PH1Tiles;


extern const short int PH2Shape;
extern const short int PH2Size;
extern const short int PH2Tiles;


extern const short int PH3Shape;
extern const short int PH3Size;
extern const short int PH3Tiles;


extern const short int PH4Shape;
extern const short int PH4Size;
extern const short int PH4Tiles;


extern const short int PM1Shape;
extern const short int PM1Size;
extern const short int PM1Tiles;


extern const short int PM2Shape;
extern const short int PM2Size;
extern const short int PM2Tiles;


extern const short int PM3Shape;
extern const short int PM3Size;
extern const short int PM3Tiles;


extern const short int PM4Shape;
extern const short int PM4Size;
extern const short int PM4Tiles;




extern const short int PickUpPallet[];

//const short int PH1Pallet;
//const short int PH2Pallet;
//const short int PH3Pallet;
//const short int PH4Pallet;
//const short int PM1Pallet;
//const short int PM2Pallet;
//const short int PM3Pallet;
//const short int PM4Pallet;

extern const unsigned int PH1[];


extern const unsigned int PM1[];



#endif
