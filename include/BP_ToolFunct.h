////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Black Paw Tool functions
//
// summary:	Generic functions for memory copy mainly
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BP_TOOLFUNCT_H
#define BP_TOOLFUNCT_H

#include "BP_ToolSet.h"
#include "BP_NoCash.h"

//These memory copy functions can be used in place of the c memcpy function to ensure that the correct
//data size is copied across. However the memcpy is fine to use.

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory copy byte. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//this should really be only used for copying data to SRAM as the lane is only
//8bits wide so only a byte copy can be performed
void BP_memcpy8(unsigned char* src, unsigned char* dst, unsigned short count);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory copy 2byte (16bit). </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_memcpy16(unsigned short* src, unsigned short* dst, unsigned short count);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory copy 4byte (32bit). </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//This is the fastest method of copying data from one destination to another
//due to the GBA being a 32bit machine
void BP_memcpy32(unsigned int* src, unsigned int* dst, unsigned short count);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Bit Masking and setting for use with anything that needs masking. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//This is a global define function but for OAM setting explained in BP_Graphics under OAM Bit Set
#define BitClr(in, pos, dat)					(in &= ~(dat << pos))
#define BitSet(in, pos, dat)					(in |= (dat << pos))
#define BitCSet(in, pos, Mask, dat)				(BitClr(in, pos, Mask) | BitSet(in, pos, dat))


////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	enum for the choice on the format for itoa returns. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//type def for the itoafunction
typedef enum IntFormats
{
	BINARY = 0,
	OCTAL,
	BASE10L,
	BASE10,
	HEX
}IntFormats;

//The itoa function has a small lut for the ascii numbers and ABCDEF
extern char asciilut[];

extern char output[33];

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	int to ascii function will return a max of 32 characters
///				A format can be set to return binary, octal, base10,
///				Hex. 0's will be included with the retunr e.g 0000000055. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

char* BP_itoa(unsigned int src, enum IntFormats type);

#endif