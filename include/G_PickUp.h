
#ifndef G_PLAYER_H
#define G_PLAYER_H

//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================
#include "G_General.h"

#include "s_Pickups.h"
//=================================

#define __PICK__ "[PickUp]"

typedef enum PickType
{
	P_NULL = 0,
	P_HEALTH = 1,
	P_DAMAGE = 2,
}PickType;

//Player Structure
//=================================
typedef struct PickFormat
{
	Vector2 Position;
	SpriteAtt* Sprite;
	u32 AnimationTime;
	u32 TimeAlive;
	bool Active;
	PickType Type;
}PickFormat;
//=================================


//Variables
//=================================
PickFormat PickUps[3]; //max  at any one time

AnimationStruct PickUpAnimation;

u32 PickUpTimer;

Ccube PickCollisionBox;
//=================================


void Pick_Initalise();

void Pick_Update();

PickType Pick_CheckCollision(Ccube* collisonBox);

#endif
