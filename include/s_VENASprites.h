//Chris Millin GBA Batch Converter


#ifndef S_VENASPRITES_H
#define S_VENASPRITES_H

extern const short int NanoShipShape;
extern const short int NanoShipSize;
extern const short int NanoShipTiles;


extern const short int NanoBulletShape;
extern const short int NanoBulletSize;
extern const short int NanoBulletTiles;


extern const short int Enemy1Shape;
extern const short int Enemy1Size;
extern const short int Enemy1Tiles;


extern const short int EnemyBulletShape;
extern const short int EnemyBulletSize;
extern const short int EnemyBulletTiles;




extern const short int Pallet0[];


extern const short int Pallet1[];


extern const short int Pallet2[];

extern const short int AIHit[];

//const short int NanoShipPallet;
//const short int NanoBulletPallet;
//const short int Enemy1Pallet;
//const short int EnemyBulletPallet;

extern const unsigned int NanoShip[];


extern const unsigned int NanoBullet[];

extern const unsigned int NanoDeath[];

extern const unsigned int Enemy1[];

extern const unsigned int EnemyExpolsion[];

extern const unsigned int EnemyBullet[];



#endif
