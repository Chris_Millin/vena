#ifndef G_BULLETMANAGER_H
#define G_BULLETMANAGER_H

//Black Paw Includes
//=================================
#include "BP_Include.h"
//=================================

//External Includes
//=================================
#include "G_General.h"
#include "s_VENASprites.h"
//=================================

#define MAX_BULLETS 70
#define BULLET_WIDTH  4
#define BULLET_HEIGHT 4
#define BULLET_X_OFFSET  3
#define BULLET_Y_OFFSET 2
#define __BULLETM__ "[BulletM]"

//NOTE
//=================================
//
// Be a bit lazy and just use a fix OAM address for bullets
// so No need to mess around with the bullet count growing and
// shrinking
// 
//=================================


//Bullet Structure
//=================================
typedef struct s8Vector2
{
	s8 x;
	s8 y;
}s8Vector2;

typedef struct BulletFormat
{
	Vector2 Position;
	SpriteAtt* Sprite;
	u16 Damage;
	s8Vector2 Direction;
	bool Active;
	bool FromPlayer;
}BulletFormat;
//=================================


//Variables
//=================================
BulletFormat Bullets[MAX_BULLETS];

//rather than waste memory making each bullet have a collision box
//use one and then offset the position each time
Ccube BulletCBox;
//=================================


void Bullets_Initalise();

void Bullets_Update();

void Bullets_Add(u16 PositionX, u16 PositionY , u16 Damage, u8 SpeedX, u8 SpeedY, bool FromPlayer);

u8 Bullet_checkCollision(Ccube* CollisionObj, bool Player);

#endif
