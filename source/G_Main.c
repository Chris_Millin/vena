#include "G_Main.h"


void G_Initalise()
{
	//Set all sprites to be hidden since they are all set to shown by default
	//==================================================================================
	BP_InitaliseOAM(OAM_Buffer, 128);
	//set the OBJ offset to 0
	OBJ_Offset = 0;
	//==================================================================================

	CompleteMessageShown = false;


	//setup the background
	//==================================================================================
	PetriBG.ScreenBase	= SCREEN_BASE_BLOCK16(31);
	PetriBG.CharBase	= CHAR_BASE_BLOCK16(0);
	PetriBG.CurrentX	= 0;
	PetriBG.CurrentY	= 0;
	PetriBG.MapSrc		= (unsigned short*)b_PetriData;
	PetriBG.MapHeight	= 20;
	PetriBG.MapWidth	= 30;
	PetriBG.TileOffSet	= 0;

	PetriBG0.ScreenBase	= SCREEN_BASE_BLOCK16(30);
	PetriBG0.CharBase	= CHAR_BASE_BLOCK16(0);
	PetriBG0.CurrentX	= 0;
	PetriBG0.CurrentY	= 0;
	PetriBG0.MapSrc		= (unsigned short*)b_BG0Data;
	PetriBG0.MapHeight	= 32;
	PetriBG0.MapWidth	= 32;
	PetriBG0.TileOffSet	= 0;

	PetriBG1.ScreenBase	= SCREEN_BASE_BLOCK16(29);
	PetriBG1.CharBase	= CHAR_BASE_BLOCK16(0);
	PetriBG1.CurrentX	= 0;
	PetriBG1.CurrentY	= 0;
	PetriBG1.MapSrc		= (unsigned short*)b_BG1Data;
	PetriBG1.MapHeight	= 32;
	PetriBG1.MapWidth	= 32;
	PetriBG1.TileOffSet	= 0;

	//load map data
	BP_memcpy16(&BGPallet, &BG_Pallet[0], 16);
	BP_memcpy16(&b_PetriTiles, &PetriBG.CharBase[0], (b_PetriTileCount * 16));

	//render the map
	BP_RenderMap(PetriBG);
	BP_RenderMap(PetriBG0);
	BP_RenderMap(PetriBG1);
	
	screenScroll1 = 2;
	screenScroll2 = 1;
	//==================================================================================




	//Initalise Game Objects
	//==================================================================================
	//Player
	Player_Initalise(100, 100);
	//Bullet Manager
	Bullets_Initalise(); //MUST INITALISE BULLETS BEFORE AI DUE TO PALLET USAGE
	//enemies
	Enemies_Initalise();
	//intalise the pickup objects
	Pick_Initalise();
	//==================================================================================
	mmStart(MOD_SONG, MM_PLAY_LOOP);
	BP_ClearText();
}

void G_Update()
{
	if (BP_isKeyPressed(Key_Start) && PauseHeld == false)
	{
		if (Paused)
		{
			BP_ClearText(); 
			Paused = false;
		}
		else
		{
			BP_ClearText(); 
			BP_PrintText("Paused", 12,10);
			BP_FinishWrite();
			Paused = true;
		}
		
		PauseHeld = true;
	}
	if (!BP_isKeyPressed(Key_Start))
		PauseHeld = false;
	

	if (!Paused)
	{
		//Update Game Objects
		//==================================================================================
		//update Player
		Player_Update();
		//update the bullet manager
		Bullets_Update();
		//update AI
		Enemies_Update();
		//update pickups
		Pick_Update();
		//==================================================================================

		//scroll map
		//==================================================================================
		screenScroll2--;
		if (screenScroll2 == 0)
		{
			screenScroll2 = 256;
		}
		screenScroll1-=2;
		if (screenScroll1 == 0)
		{
			screenScroll1 = 256;
		}
		BGScrollAdd[1].y = screenScroll1;
		BGScrollAdd[2].y = screenScroll2;
		//==================================================================================
	}
	if (GameComplete == true)
	{
		if(!CompleteMessageShown)
		{
			Player_DisbaleHealthBar();
			BP_ClearText(); 
			BP_PrintText("You're Winner", 12,10);
			BP_PrintText("Press [B] to go back to the menu", 4,12);
			BP_FinishWrite();
			CompleteMessageShown = true;
		}
	}
	if (GameFail == true)
	{
		if(!CompleteMessageShown)
		{
			BP_ClearText(); 
			BP_PrintText("GAME OVER", 12,10);
			BP_PrintText("Press [B] to go back to the menu", 4,12);
			BP_FinishWrite();
			CompleteMessageShown = true;
		}
	}

	if (BP_isKeyPressed(Key_B) && CompleteMessageShown)
	{
		mmStop();
		mmFrame();
		BP_BlockOut();
		ShowMenu();
	}

}

void G_Draw()
{
	//copies over the sprite buffer into the sprite memory address which will cause them to be
	//re-drawn in the new positions
	BP_OAMCpy(OAMAddress, OAM_Buffer, 128);
}

void ShowMenu()
{
	OBJ_Offset = 0;
	BGScrollAdd[1].y = 0;
	BGScrollAdd[2].y = 0;

	/// <summary>	Sets The Graphics mode for sprites and OBJs. </summary>
	GFX_DISPCNT = (GFX_MODE0 | GFX_OBJ_1D | GFX_OBJ_On | GFX_BG0_On | GFX_BG1_On);
	//=================================
	WFormat Font;

	Font.ScreenBase 	= SCREEN_BASE_BLOCK16(30);
	Font.CharBase	  	= &CHAR_BASE_BLOCK16(1)[8];
	Font.MaxTiles	  	= 100;
	Font.PalletColours 	= BG_Pallet[16]; //tmp
	Font.FontType 		= &BP_FontData[0];
	//=================================

	//BP_InitaliseOAM(&OAM_Buffer[0], 128);

	BP_InitaliseWriter(Font);

	BP_ShowLogo();

	BP_ClearText();

	Menu_Initalise();

	BP_BlockOut();

	while(Menu_Update()) {mmFrame();}

	BP_ClearText(); 

	BP_BlockIn();

	/// <summary>	Sets The Graphics mode for sprites and OBJs. </summary>
	GFX_DISPCNT = (GFX_MODE0 | GFX_OBJ_1D | GFX_OBJ_On | GFX_BG0_On | GFX_BG1_On | GFX_BG2_On | GFX_BG3_On);

	/// <summary>	Sets BG0 to use chaacter base block 31 for rendering. </summary>
	BGREG0 = (31 << 8) | (1 << 6) | 1;
	/// <summary>	Sets BG1 to use chaacter base block 29 for rendering. </summary>
	BGREG1 = (29 << 8) | (1 << 6) | 2;
	/// <summary>	Sets BG2 to use chaacter base block 30 for rendering. </summary>
	BGREG2 = (30 << 8) | (1 << 6) | 3;
	/// <summary>	Sets BG3 to use chaacter base block 28 for rendering at character block 1. </summary>
	BGREG3 = (28 << 8) | (1 << 6) | (1 << 2) | 0;

	Font.ScreenBase 	= SCREEN_BASE_BLOCK16(28);
	Font.CharBase	  	= &CHAR_BASE_BLOCK16(1)[8];
	Font.MaxTiles	  	= 100;
	Font.PalletColours 	= BG_Pallet[16]; //tmp
	Font.FontType 		= &BP_FontData[0];
	//=================================

	BP_InitaliseOAM(&OAM_Buffer[0], 128);

	BP_InitaliseWriter(Font);

	srand(Frame_Count * (Frame_Count * 3));

	G_Initalise();

	BP_BlockOut();

	GameComplete = false;
	GameFail = false;

}