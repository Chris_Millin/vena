//Chris Millin GBA Batch Converter


#include "s_Pickups.h"


const short int PH1Shape = 0;
const short int PH1Size = 0;
const short int PH1Tiles = 1;


const short int PH2Shape = 0;
const short int PH2Size = 0;
const short int PH2Tiles = 1;


const short int PH3Shape = 0;
const short int PH3Size = 0;
const short int PH3Tiles = 1;


const short int PH4Shape = 0;
const short int PH4Size = 0;
const short int PH4Tiles = 1;


const short int PM1Shape = 0;
const short int PM1Size = 0;
const short int PM1Tiles = 1;


const short int PM2Shape = 0;
const short int PM2Size = 0;
const short int PM2Tiles = 1;


const short int PM3Shape = 0;
const short int PM3Size = 0;
const short int PM3Tiles = 1;


const short int PM4Shape = 0;
const short int PM4Size = 0;
const short int PM4Tiles = 1;




const short int PickUpPallet[] = {
0x7c1f,
0x001f,
0x001f,
0x001f,
0x5d60,
0x4900,
0x4500,
0x001f,
0x080d,
0x0bfe,
0x7fff,
0x6980,
0x1015,
0x0c12,
0x0811,
0x080e,
};

//Sprite PH1 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PH1Pallet = 0;
//Sprite PH2 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PH2Pallet = 0;
//Sprite PH3 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PH3Pallet = 0;
//Sprite PH4 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PH4Pallet = 0;
//Sprite PM1 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PM1Pallet = 0;
//Sprite PM2 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PM2Pallet = 0;
//Sprite PM3 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PM3Pallet = 0;
//Sprite PM4 uses pallet 0
//uncomment bellow and in header for pallet variable
//const short int PM4Pallet = 0;

const unsigned int PH1[] = {
0x0000,
0x888800,
0x8AFFA80,
0xFAEEAF0,
0xDAAAAD0,
0xCADDAC0,
0xCCCC00,
0x0000,
//PH2
0x0000,
0x9888890,
0x8AFFA80,
0xFAEEAF0,
0xDAAAAD0,
0xCADDAC0,
0x9CCCC90,
0x0000,
//PH3
0x999900,
0x9888890,
0x98AFFA89,
0x9FAEEAF9,
0x9DAAAAD9,
0x9CADDAC9,
0x9CCCC90,
0x999900,
//PH4
0x99000099,
0x90888809,
0x8AFFA80,
0xFAEEAF0,
0xDAAAAD0,
0xCADDAC0,
0x90CCCC09,
0x99000099,
};


const unsigned int PM1[] = {
0x0000,
0x666600,
0x6055060,
0x5400450,
0x4B00B40,
0xB0440B0,
0xBBBB00,
0x0000,
//PM2
0x0000,
0x9666690,
0x6055060,
0x5400450,
0x4B00B40,
0xB0440B0,
0x9BBBB90,
0x0000,
//PM3
0x999900,
0x9666690,
0x96055069,
0x95400459,
0x94B00B49,
0x9B0440B9,
0x9BBBB90,
0x999900,
//PM4
0x99000099,
0x90666609,
0x6055060,
0x5400450,
0x4B00B40,
0xB0440B0,
0x90BBBB09,
0x99000099,
};

