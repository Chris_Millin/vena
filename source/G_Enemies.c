#include "G_Enemies.h"

void Enemies_Initalise()
{
	//hit pallet
	BP_memcpy16(&AIHit, &OBJPallet[48], 16); //Copy Over Bullet Pallet

	//set the OBJ offset
	OBJEnemyOffset = OBJ_Offset;
	u8 x = 0;
	for (x = 0; x < MAX_ENEMIES; x++)
	{
		Enemies[x].Active = false;
		Enemies[x].Health = 70;
		Enemies[x].PreventNew = false;
		Enemies[x].Died = 100;
		Enemies[x].Evolution = 0;
		Enemies[x].Sprite = 0;
		Enemies[x].Damaged = 0;
		Enemies[x].AnimationTime = 0;
		Enemies[x].CreationTime = 0;
		Enemies[x].fakeY = 300;
		Enemies[x].Zeroed = false;
		Enemies[x].Type = 0;
	}

	//first set
	//==================================================================================
	u8 i = 0;
	for (i = 0; i < MIN_ENEMIES; i++)
	{
		Enemies[i].Active = true;
		BP_memcpy32(&Enemy1, &OBJAdd32[OBJ_Offset], (Enemy1Tiles*8));
		OBJ_Offset+= (Enemy1Tiles*8);
		Enemies[i].Type = rand() % 5 + 1;
		
		Enemies[i].Position.y = 0;
		Enemies[i].fakeY = rand() % 230 + 30;
		//Enemy Generations
		//==================================================================================
		switch (Enemies[i].Type)
		{
			case 1:
				Enemies[i].Position.x = rand() % 204 + 2;
				break;
			case 2:
				Enemies[i].Position.x = 0;
				break;
			case 3:
				Enemies[i].Position.x = 232;
				break;
			case 4:
				Enemies[i].Position.x = rand() % 204 + 2;
				break;
			case 5:
				Enemies[i].Position.x = rand() % 204 + 2;
				break;
		}
		//==================================================================================

		Enemies[i].Died = 0;
		Enemies[i].Active = false;
		Enemies[i].Zeroed = false;
		Enemies[i].Health = 70;
		Enemies[i].Damaged = Frame_Count - 8;
		Enemies[i].Sprite = &OAM_Buffer[i];
		BP_SetOAM(Enemies[i].Sprite, OAMATT0_Shape_Square, OAMATT1_Size_32, Set_ATTR2((i*16) + 18, 2, 2));
		BP_SetOAMPos(Enemies[i].Sprite, Enemies[i].Position.y, Enemies[i].Position.x);
		BP_EnableOAM(Enemies[i].Sprite, false);
	}
	//==================================================================================


	//Create the animation struct for the AI
	//==================================================================================
	EnemyAnimation.startAdd = 0;
	EnemyAnimation.TileCount = 16;
	EnemyAnimation.Frames = 4;
	EnemyAnimation.FrameDuration = 16;

	EnemyExplosionAnimation.startAdd = 0;
	EnemyExplosionAnimation.TileCount = 16;
	EnemyExplosionAnimation.Frames = 5;
	EnemyExplosionAnimation.FrameDuration = 4;
	//==================================================================================


	KillCount = 0;
}



void Enemies_Update()
{
	u8 Active = 0;
	u8 Dead = 0;
	
	u8 GameOver = 0;
	//update all the enemy types
	//==================================================================================
	u8 i = 0;
	for (i = 0; i < MAX_ENEMIES; i++)
	{
		if (Enemies[i].PreventNew == true)
		{
			GameOver++;
		}
		else
		{
			Enemies_Evolve(i);

			if ((Frame_Count - Enemies[i].Damaged) >= 8)
			{
				//reset pallet after hit
				BP_SetSprite(Enemies[i].Sprite, ((i*16) + 18), 2);
			}
		
			switch (Enemies[i].Type)
			{
			case 1:
				Enemies_T1Update(i);
				break;
			case 2:
				Enemies_T2Update(i);
				break;
			case 3:
				Enemies_T3Update(i);
				break;
			case 4:
				Enemies_T4Update(i);
				break;
			case 5:
				Enemies_T5Update(i);
				break;
			}

			//collision check
			//==================================================================================
			CollisionBox.x = Enemies[i].Position.x + 7;
			CollisionBox.y = Enemies[i].Position.y + 9;
			CollisionBox.width = 18;
			CollisionBox.height = 16;

			//health before
			s16 healthbefore = Enemies[i].Health;

			Enemies[i].Health -= Bullet_checkCollision(&CollisionBox, false);

			if (healthbefore > Enemies[i].Health)
			{
				BP_SetSprite(Enemies[i].Sprite, ((i*16) + 18), 3);
				Enemies[i].Damaged = Frame_Count;
				//mmEffect(MOD_HIT);
			}

			if (Enemies[i].Health <= 0 && Enemies[i].Active == true)
			{

				Enemies[i].Active = false;
				Enemies[i].AnimationTime = Frame_Count;	
				mmEffect(MOD_EXPLOSION);
			}

			//==================================================================================

			if (Enemies[i].Active == true)
			{
				if(BP_RunAnimation(&Enemy1, Enemies[i].AnimationTime, &OBJAdd32[OBJEnemyOffset + (i * (Enemy1Tiles*8))], &EnemyAnimation))
					Enemies[i].AnimationTime = Frame_Count;
			}
			if(Enemies[i].Active == false & Enemies[i].Health <= 0 )
			{
				if(BP_RunAnimation(&EnemyExpolsion, Enemies[i].AnimationTime, &OBJAdd32[OBJEnemyOffset + (i * (Enemy1Tiles*8))], &EnemyExplosionAnimation))
				{
					BP_EnableOAM(Enemies[i].Sprite, false);
					Enemies[i].Died++;
					Enemies_GenNew(i);
				}
			}
			
		}
	}

	if (GameOver == MAX_ENEMIES)
		GameComplete = true;

	//==================================================================================
}



void Enemies_GenNew(u8 ID)
{
	if(Enemies[ID].PreventNew != true)
	{
		
		Enemies[ID].Type = rand() % 5 + 1;
		
		Enemies[ID].Position.y = 0;
		Enemies[ID].fakeY = rand() % 230 + 30;
		//Enemy Generations
		//==================================================================================
		switch (Enemies[ID].Type)
		{
		case 1:
			if (Enemies[ID].Evolution >= 4)
				Enemies[ID].Position.x = rand() % 40 + (Player.Position.x - 20);
			else
				Enemies[ID].Position.x = rand() % 204 + 2;
			break;
		case 2:
			Enemies[ID].Position.x = 0;
			break;
		case 3:
			Enemies[ID].Position.x = 232;
			break;
		case 4:
			if (Enemies[ID].Evolution >= 4)
				Enemies[ID].Position.x = rand() % 40 + (Player.Position.x - 20);
			else
				Enemies[ID].Position.x = rand() % 204 + 2;
			break;
		case 5:
			if (Enemies[ID].Evolution >= 4)
				Enemies[ID].Position.x = rand() % 40 + (Player.Position.x - 20);
			else
				Enemies[ID].Position.x = rand() % 204 + 2;
			break;
		}
		//==================================================================================

		Enemies[ID].Active = false;
		Enemies[ID].Zeroed = false;
		Enemies[ID].Health = 70;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		BP_EnableOAM(Enemies[ID].Sprite, false);
	}
}


void Enemies_AddNew()
{

	u8 ID = 0;
	for (ID = 0; ID < MAX_ENEMIES; ID++)
	{
		
		if (Enemies[ID].Active == false && Enemies[ID].PreventNew == false && Enemies[ID].Died == 100)
		{
			Enemies[ID].Type = rand() % 5 + 1;

			Enemies[ID].Position.y = 0;
			Enemies[ID].fakeY = rand() % 230 + 30;
			//Enemy Generations
			//==================================================================================
			switch (Enemies[ID].Type)
			{
			case 1:
				Enemies[ID].Position.x = rand() % 204 + 2;
				break;
			case 2:
				Enemies[ID].Position.x = 0;
				break;
			case 3:
				Enemies[ID].Position.x = 232;
				break;
			case 4:
				Enemies[ID].Position.x = rand() % 204 + 2;
				break;
			case 5:
				Enemies[ID].Position.x = rand() % 204 + 2;
				break;
			}
			//==================================================================================
			
			Enemies[ID].Died = 0;
			Enemies[ID].Active = false;
			Enemies[ID].Zeroed = false;
			Enemies[ID].Health = 70;
			Enemies[ID].Damaged = Frame_Count - 8;
			Enemies[ID].Sprite = &OAM_Buffer[ID];
			BP_SetOAM(Enemies[ID].Sprite, OAMATT0_Shape_Square, OAMATT1_Size_32, Set_ATTR2((ID*16) + 18, 2, 2));
			BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
			BP_EnableOAM(Enemies[ID].Sprite, false);
			break;
		}
	}
	
}

void Enemies_T1Update(u8 ID)
{
	//Has the Fake Y reached 0 or less if not decrement it
	//==================================================================================
	if (Enemies[ID].fakeY > 0 && Enemies[ID].fakeY < 300)
		Enemies[ID].fakeY-=2;
	//==================================================================================


	//if AI is not active
	//==================================================================================
	if (Enemies[ID].fakeY <= 0 & Enemies[ID].Active == false)
	{
		Enemies[ID].fakeY = 300;
		Enemies[ID].Active = true;
		Enemies[ID].Position.y = 224;
		Enemies[ID].CreationTime = Frame_Count;
		Enemies[ID].AnimationTime= Frame_Count;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		BP_EnableOAM(Enemies[ID].Sprite, true);
	}
	//==================================================================================



	//if the AI has just been activate it will be at 224 and active
	// add untill position is 256 then zero the AI
	//==================================================================================
	if(Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true )
	{
		Enemies[ID].Position.y++;
		if(Enemies[ID].Position.y >= 256)
		{
			Enemies[ID].Position.y = 0;
			Enemies[ID].Zeroed = true;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) % 35 == 0)
			Bullets_Add(Enemies[ID].Position.x + 12, 256 - Enemies[ID].Position.y, 1, 0, 2, false);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);	
	}
	//==================================================================================


	//new AI can run as normal
	//==================================================================================
	if (Enemies[ID].Position.y < 224 & Enemies[ID].Active == true)
	{
		Enemies[ID].Position.y++;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		switch(Enemies[ID].Evolution)
		{
		case 1:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 5, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 45 == 0)
				Bullets_Add(Enemies[ID].Position.x + 27, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			break;
		case 2:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			break;
		case 3:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 0, false);
			break;
		case 5:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 2, false);
			break;
		}
	}
	//==================================================================================
	

	//Gen New AI if it has come to the bottom of the screen
	//==================================================================================
	if (Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true & Enemies[ID].Zeroed == true)
		Enemies_GenNew(ID);
	//==================================================================================
}





void Enemies_T2Update(u8 ID)
{
	//Has the Fake Y reached 0 or less if not decrement it
	//==================================================================================
	if (Enemies[ID].fakeY > 0 && Enemies[ID].fakeY < 300)
		Enemies[ID].fakeY-=2;
	//==================================================================================


	//if AI is not active
	//==================================================================================
	if (Enemies[ID].fakeY <= 0 & Enemies[ID].Active == false)
	{
		Enemies[ID].fakeY = 300;
		Enemies[ID].Active = true;
		Enemies[ID].Position.y = 224;
		Enemies[ID].CreationTime = Frame_Count;
		Enemies[ID].AnimationTime= Frame_Count;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		BP_EnableOAM(Enemies[ID].Sprite, true);
	}
	//==================================================================================



	//if the AI has just been activate it will be at 224 and active
	// add untill position is 256 then zero the AI
	//==================================================================================
	if(Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true )
	{
		Enemies[ID].Position.y++;
		Enemies[ID].Position.x++;
		if (Enemies[ID].Position.x > 240)
			Enemies_GenNew(ID);
		if(Enemies[ID].Position.y >= 256)
		{
			Enemies[ID].Position.y = 0;
			Enemies[ID].Zeroed = true;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) % 35 == 0)
			Bullets_Add(Enemies[ID].Position.x + 12, 256 - Enemies[ID].Position.y, 1, 0, 2, false);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);	
	}
	//==================================================================================


	//new AI can run as normal
	//==================================================================================
	if (Enemies[ID].Position.y < 224 & Enemies[ID].Active == true)
	{
		Enemies[ID].Position.y++;
		Enemies[ID].Position.x++;
		if (Enemies[ID].Position.x > 240)
			Enemies_GenNew(ID);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		switch(Enemies[ID].Evolution)
		{
		case 1:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 5, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 45 == 0)
				Bullets_Add(Enemies[ID].Position.x + 27, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			break;
		case 2:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			break;
		case 3:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 0, false);
			break;
		case 5:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 2, false);
			break;
		}
	}
	//==================================================================================


	//Gen New AI if it has come to the bottom of the screen
	//==================================================================================
	if (Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true & Enemies[ID].Zeroed == true)
		Enemies_GenNew(ID);
	//==================================================================================
}





void Enemies_T3Update(u8 ID)
{
	//Has the Fake Y reached 0 or less if not decrement it
	//==================================================================================
	if (Enemies[ID].fakeY > 0 && Enemies[ID].fakeY < 300)
		Enemies[ID].fakeY-=2;
	//==================================================================================


	//if AI is not active
	//==================================================================================
	if (Enemies[ID].fakeY <= 0 & Enemies[ID].Active == false)
	{
		Enemies[ID].fakeY = 300;
		Enemies[ID].Active = true;
		Enemies[ID].Position.y = 224;
		Enemies[ID].CreationTime = Frame_Count;
		Enemies[ID].AnimationTime= Frame_Count;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		BP_EnableOAM(Enemies[ID].Sprite, true);
	}
	//==================================================================================



	//if the AI has just been activate it will be at 224 and active
	// add untill position is 256 then zero the AI
	//==================================================================================
	if(Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true )
	{
		Enemies[ID].Position.y++;
		Enemies[ID].Position.x--; 
		if (Enemies[ID].Position.x <= 2)
			Enemies_GenNew(ID);
		if(Enemies[ID].Position.y >= 256)
		{
			Enemies[ID].Position.y = 0;
			Enemies[ID].Zeroed = true;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) % 35 == 0)
			Bullets_Add(Enemies[ID].Position.x + 12, 256 - Enemies[ID].Position.y, 1, 0, 2, false);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);	
	}
	//==================================================================================


	//new AI can run as normal
	//==================================================================================
	if (Enemies[ID].Position.y < 224 & Enemies[ID].Active == true)
	{
		Enemies[ID].Position.y++;
		Enemies[ID].Position.x--; 
		if (Enemies[ID].Position.x <= 2)
			Enemies_GenNew(ID);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		switch(Enemies[ID].Evolution)
		{
		case 1:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 5, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 45 == 0)
				Bullets_Add(Enemies[ID].Position.x + 27, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			break;
		case 2:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			break;
		case 3:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 0, false);
			break;
		case 5:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 2, false);
			break;
		}
	}
	//==================================================================================


	//Gen New AI if it has come to the bottom of the screen
	//==================================================================================
	if (Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true & Enemies[ID].Zeroed == true)
		Enemies_GenNew(ID);
	//==================================================================================
}




void Enemies_T4Update(u8 ID)
{
	//Has the Fake Y reached 0 or less if not decrement it
	//==================================================================================
	if (Enemies[ID].fakeY > 0 && Enemies[ID].fakeY < 300)
		Enemies[ID].fakeY-=2;
	//==================================================================================


	//if AI is not active
	//==================================================================================
	if (Enemies[ID].fakeY <= 0 & Enemies[ID].Active == false)
	{
		Enemies[ID].fakeY = 300;
		Enemies[ID].Active = true;
		Enemies[ID].Position.y = 224;
		Enemies[ID].CreationTime = Frame_Count;
		Enemies[ID].AnimationTime= Frame_Count;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		BP_EnableOAM(Enemies[ID].Sprite, true);
	}
	//==================================================================================



	//if the AI has just been activate it will be at 224 and active
	// add untill position is 256 then zero the AI
	//==================================================================================
	if(Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true )
	{
		Enemies[ID].Position.y++;
		if(Enemies[ID].Position.y >= 256)
		{
			Enemies[ID].Position.y = 0;
			Enemies[ID].Zeroed = true;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) % 35 == 0)
			Bullets_Add(Enemies[ID].Position.x + 12, 256 - Enemies[ID].Position.y, 1, 0, 2, false);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);	
	}
	//==================================================================================


	//new AI can run as normal
	//==================================================================================
	if (Enemies[ID].Position.y < 224 & Enemies[ID].Active == true)
	{
		if ((Frame_Count - Enemies[ID].CreationTime) < 50)
		{
			Enemies[ID].Position.y++;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) >= 100)
		{
			Enemies[ID].Position.x--;
			if (Enemies[ID].Position.x == 0 | Bullets[ID].Position.y > 65505)
				Enemies_GenNew(ID);
		}
		
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		switch(Enemies[ID].Evolution)
		{
		case 1:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 5, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 45 == 0)
				Bullets_Add(Enemies[ID].Position.x + 27, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			break;
		case 2:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			break;
		case 3:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 0, false);
			break;
		case 5:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 2, false);
			break;
		}
	}
	//==================================================================================


	//Gen New AI if it has come to the bottom of the screen
	//==================================================================================
	if (Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true & Enemies[ID].Zeroed == true)
		Enemies_GenNew(ID);
	//==================================================================================
}




void Enemies_T5Update(u8 ID)
{
	//Has the Fake Y reached 0 or less if not decrement it
	//==================================================================================
	if (Enemies[ID].fakeY > 0 && Enemies[ID].fakeY < 300)
		Enemies[ID].fakeY-=2;
	//==================================================================================


	//if AI is not active
	//==================================================================================
	if (Enemies[ID].fakeY <= 0 & Enemies[ID].Active == false)
	{
		Enemies[ID].fakeY = 300;
		Enemies[ID].Active = true;
		Enemies[ID].Position.y = 224;
		Enemies[ID].CreationTime = Frame_Count;
		Enemies[ID].AnimationTime= Frame_Count;
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		BP_EnableOAM(Enemies[ID].Sprite, true);
	}
	//==================================================================================



	//if the AI has just been activate it will be at 224 and active
	// add untill position is 256 then zero the AI
	//==================================================================================
	if(Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true )
	{
		Enemies[ID].Position.y++;
		if(Enemies[ID].Position.y >= 256)
		{
			Enemies[ID].Position.y = 0;
			Enemies[ID].Zeroed = true;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) % 35 == 0)
			Bullets_Add(Enemies[ID].Position.x + 12, 256 - Enemies[ID].Position.y, 1, 0, 2, false);
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);	
	}
	//==================================================================================


	//new AI can run as normal
	//==================================================================================
	if (Enemies[ID].Position.y < 224 & Enemies[ID].Active == true)
	{
		if ((Frame_Count - Enemies[ID].CreationTime) < 50)
		{
			Enemies[ID].Position.y++;
		}
		if ((Frame_Count - Enemies[ID].CreationTime) >= 100)
		{
			Enemies[ID].Position.x++;
			if (Enemies[ID].Position.x > 240)
				Enemies_GenNew(ID);
		}
		BP_SetOAMPos(Enemies[ID].Sprite, Enemies[ID].Position.y, Enemies[ID].Position.x);
		switch(Enemies[ID].Evolution)
		{
		case 1:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 5, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 45 == 0)
				Bullets_Add(Enemies[ID].Position.x + 27, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			break;
		case 2:
			if ((Frame_Count - Enemies[ID].CreationTime) % 40 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			break;
		case 3:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 0, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 55 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 0, false);
			break;
		case 5:
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 0, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, -2, 2, false);
			if ((Frame_Count - Enemies[ID].CreationTime) % 50 == 0)
				Bullets_Add(Enemies[ID].Position.x + 12, Enemies[ID].Position.y + 32, 1, 2, 2, false);
			break;
		}
	}
	//==================================================================================


	//Gen New AI if it has come to the bottom of the screen
	//==================================================================================
	if (Enemies[ID].Position.y >= 224 & Enemies[ID].Active == true & Enemies[ID].Zeroed == true)
		Enemies_GenNew(ID);
	//==================================================================================
}


void Enemies_Evolve(u8 ID)
{
	if (Enemies[ID].Died == 2)
	{

		Enemies[ID].Evolution = 1;
	}
	if (Enemies[ID].Died == 3)
	{
		Enemies[ID].Died++; //cheap trick to prevent it from doing this multiple time
		Enemies_AddNew();
	}
	if (Enemies[ID].Died == 5)
	{
		Enemies[ID].Evolution = 2;
	}
	if (Enemies[ID].Died == 7)
	{
		Enemies[ID].Evolution = 3;
	}
	if (Enemies[ID].Died == 9)
	{
		Enemies[ID].Evolution = 4;
	}
	if (Enemies[ID].Died == 11)
	{
		Enemies[ID].Evolution = 5;
	}
	if (Enemies[ID].Died == 14)
	{
		BP_EnableOAM(Enemies[ID].Sprite, false);
		Enemies[ID].PreventNew = true;
	}

}