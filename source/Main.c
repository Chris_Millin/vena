

//Other Lib Includes
//=================================
#include <maxmod.h>		// maxmod library
#include "soundbank.h"		// created by building project
#include "soundbank_bin.h"	// created by building project
#include <gba_interrupt.h>
#include <gba_console.h>
#include <gba_systemcalls.h>
//=================================

//Black Paw Includes
//=================================
#include "BP_ToolSet.h"
#include "BP_ToolFunct.h"
#include "BP_Graphics.h"
#include "BP_Irq.h"
#include "BP_Input.h"
#include "BP_NoCash.h"
#include "BP_Writer.h"
#include "BP_Collision.h"
#include "BP_Font.h"
#include "BP_LogoOut.h"
//=================================

//External Includes
//=================================
#include "G_Main.h"
#include "G_BulletManager.h"
#include "G_Enemies.h"
#include "G_General.h"
#include "G_Menu.h"
//=================================



////////////////////////////////////////////////////////////////////////////////
//                         Global Definitions                                 //
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
//                            Prototypes                                      //
////////////////////////////////////////////////////////////////////////////////

void FrameTimer()
{
	//defined in Toolset so I can be used globaly
	Frame_Count++;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Main entry-point for this application. </summary>
///
/// <remarks>	Chris, 10/06/2012. </remarks>
///
/// <returns>	Exit-code for the process - 0 for success, else an error code. </returns>
////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void) {
		Frame_Count = 0;
		// the vblank interrupt must be enabled for VBlankIntrWait() to work since the default
		// dispatcher handles the bios flags no vblank handler is required.
		irqInit();
		////wait for the vblank to play audio *I believe thats how it happens
		irqSet(IRQ_VBLANK, mmVBlank);
		mmSetVBlankHandler(FrameTimer);
		irqEnable(IRQ_VBLANK);
		
		mmInitDefault((mm_addr)soundbank_bin, 8);
		mmSetModuleVolume(500);

		/// <summary>	Sets The Graphics mode for sprites and OBJs. </summary>
		GFX_DISPCNT = (GFX_MODE0 | GFX_OBJ_1D | GFX_OBJ_On | GFX_BG0_On | GFX_BG1_On);

		//=================================
		WFormat Font;

		Font.ScreenBase 	= SCREEN_BASE_BLOCK16(30);
		Font.CharBase	  	= &CHAR_BASE_BLOCK16(1)[8];
		Font.MaxTiles	  	= 100;
		Font.PalletColours 	= BG_Pallet[16]; //tmp
		Font.FontType 		= &BP_FontData[0];
		//=================================

		//BP_InitaliseOAM(&OAM_Buffer[0], 128);

		BP_InitaliseWriter(Font);

		BP_ShowLogo();

		BP_ClearText();

		Menu_Initalise();

		BP_BlockOut();

		while(Menu_Update()) {}

		BP_BlockIn();

		/// <summary>	Sets The Graphics mode for sprites and OBJs. </summary>
		GFX_DISPCNT = (GFX_MODE0 | GFX_OBJ_1D | GFX_OBJ_On | GFX_BG0_On | GFX_BG1_On | GFX_BG2_On | GFX_BG3_On);

		/// <summary>	Sets BG0 to use chaacter base block 31 for rendering. </summary>
		BGREG0 = (31 << 8) | (1 << 6) | 1;
		/// <summary>	Sets BG1 to use chaacter base block 29 for rendering. </summary>
		BGREG1 = (29 << 8) | (1 << 6) | 2;
		/// <summary>	Sets BG2 to use chaacter base block 30 for rendering. </summary>
		BGREG2 = (30 << 8) | (1 << 6) | 3;
		/// <summary>	Sets BG3 to use chaacter base block 28 for rendering at character block 1. </summary>
		BGREG3 = (28 << 8) | (1 << 6) | (1 << 2) | 0;

		Font.ScreenBase 	= SCREEN_BASE_BLOCK16(28);
		Font.CharBase	  	= &CHAR_BASE_BLOCK16(1)[8];
		Font.MaxTiles	  	= 100;
		Font.PalletColours 	= BG_Pallet[16]; //tmp
		Font.FontType 		= &BP_FontData[0];
		//=================================

		//BP_InitaliseOAM(&OAM_Buffer[0], 128);

		BP_InitaliseWriter(Font);

		srand(Frame_Count * (Frame_Count * 3));

		G_Initalise();

		BP_BlockOut();

		GameComplete = false;
		GameFail = false;

		while(1)
		{
			
			BP_UpdateKeys();
			G_Update();
			G_Draw();

			mmFrame();
			BP_VBlankWait();
		}
}