#include "G_Player.h"

void Player_Initalise(u16 PosX, u16 PosY)
{
	//set Player Position
	//==================================================================================
	Player.Position.x = PosX;
	Player.Position.y = PosY;
	//==================================================================================

	//set the health and evolution stage
	//==================================================================================
	Player.Health = 16;
	Player.Evolution = 0;
	Player.AnimationTime = Frame_Count;
	Player.BulletDamage = 30;
	PlayerEnabled = true;
	//==================================================================================

	//set the collision box of the player which is a bit smaller than him and has
	//an offset from his X and Y
	//==================================================================================
	Player.CollisionBox.x = PosX+15;
	Player.CollisionBox.y = PosY+11;
	Player.CollisionBox.width	= 3;
	Player.CollisionBox.height	= 5;
	//==================================================================================


	//Loads the player sprite object and sets the position of the object
	//==================================================================================
	//Pallet0 will need to change
	BP_memcpy16(&Pallet0, &OBJPallet[0], 16);
	BP_memcpy32(&NanoShip, &OBJAdd32[OBJ_Offset], (NanoShipTiles*8));
	OBJPlayerOffset = OBJ_Offset;
	OBJ_Offset+=(NanoShipTiles*8);
	Player.Sprite = &OAM_Buffer[127];
	BP_SetOAM(Player.Sprite, OAMATT0_Shape_Square, OAMATT1_Size_32, Set_ATTR2(0, 2, 0));
	BP_SetOAMPos(Player.Sprite, Player.Position.y, Player.Position.x);
	//==================================================================================

	PlayerAnimation.startAdd = 0;
	PlayerAnimation.TileCount = 16;
	PlayerAnimation.Frames = 12;
	PlayerAnimation.FrameDuration = 3;

	PlayerDeathAnimation.startAdd = 0;
	PlayerDeathAnimation.TileCount = 16;
	PlayerDeathAnimation.Frames = 7;
	PlayerDeathAnimation.FrameDuration = 4;

	ShootTimer = Frame_Count;

	//Create the health sprite
	//==================================================================================
	BP_memcpy16(&HealthPallet, &OBJPallet[64], 16);
	BP_memcpy32(&H1, &OBJAdd32[1424], (H1Tiles*8));
	HealthSprite = &OAM_Buffer[126];
	BP_SetOAM(HealthSprite, OAMATT0_Shape_Wide, OAMATT1_Size_32, Set_ATTR2(178, 0, 4));
	BP_SetOAMPos(HealthSprite, 2, 206);
	//==================================================================================

}

//keydown hack since it should be added to the library
bool A_Down = false;


void Player_Update()
{
	//update movement
	//==================================================================================
	if (PlayerEnabled == true)
	{
	
		if (BP_isKeyPressed(Key_Left))
		{
			Player.Position.x-=2;
		}
		if (BP_isKeyPressed(Key_Right))
		{
			Player.Position.x+=2;
		}
		if (BP_isKeyPressed(Key_Up))
		{
			Player.Position.y-=2;
		}
		if (BP_isKeyPressed(Key_Down))
		{
			Player.Position.y+=2;
		}
		if (BP_isKeyPressed(Key_A) & A_Down == false)
		{
			if ((Frame_Count - ShootTimer) >= 5)
			{
				mmEffect(MOD_SHOOT);
				ShootTimer = Frame_Count;
				Bullets_Add(Player.Position.x + 12, Player.Position.y, Player.BulletDamage, 0, -2, true);
			}	
		}
	


		//bound testing
		//==================================================================================
		if ((Player.Position.x == 0 | Player.Position.x > 65505))
			Player.Position.x = 0;
		if (Player.Position.x > 218 & Player.Position.x < 240)
			Player.Position.x = 218;
		if ((Player.Position.y == 0 | Player.Position.y > 65505))
			Player.Position.y = 0;
		if (Player.Position.y > 128 & Player.Position.y < 160)
			Player.Position.y = 128;
		//==================================================================================
	


		//Collision Check
		//==================================================================================
		Player.CollisionBox.x = Player.Position.x+15;
		Player.CollisionBox.y = Player.Position.y+11;
		u16 healthbefore = Player.Health;
		Player.Health-=Bullet_checkCollision(&Player.CollisionBox, true);
		if (healthbefore > Player.Health)
		{
			UpdateHealth();
			Player.BulletDamage-=10;
			if (Player.BulletDamage <= 10)
			{
				Player.BulletDamage = 10;
			}
		
		}	

		u8 type = Pick_CheckCollision(&Player.CollisionBox);
		if (type == 1)
		{
			mmEffect(MOD_ITEM);
			if (Player.Health < 16)
			{
				Player.Health++;
				UpdateHealth();
			}
		}
		if (type == 2)
		{
			mmEffect(MOD_ITEM);
			if (Player.BulletDamage < 100)
				Player.BulletDamage+=10;
	
		}

		//==================================================================================


	
		if (BP_RunAnimation(&NanoShip, Player.AnimationTime, &OBJAdd32[OBJPlayerOffset], &PlayerAnimation))
			Player.AnimationTime = Frame_Count;	




		//update the player sprite position
		BP_SetOAMPos(Player.Sprite, Player.Position.y, Player.Position.x);
		//==================================================================================
	}
	else
	{
		if (BP_RunAnimation(&NanoDeath, Player.AnimationTime, &OBJAdd32[OBJPlayerOffset], &PlayerDeathAnimation))
		{
			GameFail = true;
			BP_EnableOAM(Player.Sprite, false);
		}
	}
}


void UpdateHealth()
{
	if (Player.Health == 15)
	{
		BP_memcpy32(&H2, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 14)
	{
		BP_memcpy32(&H3, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 13)
	{
		BP_memcpy32(&H4, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 12)
	{
		BP_memcpy32(&H5, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 11)
	{
		BP_memcpy32(&H6, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 10)
	{
		BP_memcpy32(&H7, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 9)
	{
		BP_memcpy32(&H8, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 8)
	{
		BP_memcpy32(&H9, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 7)
	{
		BP_memcpy32(&H10, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 6)
	{
		BP_memcpy32(&H11, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 5)
	{
		BP_memcpy32(&H12, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 4)
	{
		BP_memcpy32(&H13, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 3)
	{
		BP_memcpy32(&H14, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 2)
	{
		BP_memcpy32(&H15, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 1)
	{
		BP_memcpy32(&H16, &OBJAdd32[1424], (H1Tiles*8));
	}
	if (Player.Health == 0)
	{
		PlayerEnabled = false;
		Player.AnimationTime = Frame_Count;
		BP_EnableOAM(HealthSprite, false);
		mmEffect(MOD_EXPLOSION);
	}
	
}

void Player_DisbaleHealthBar()
{
	BP_EnableOAM(HealthSprite, false);
}