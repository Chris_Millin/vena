////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Text Writer
//
// summary:	Will print a variable width font to the screen
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "BP_Writer.h"

bool Rendered;

unsigned int CharBuffer[32];

void BP_InitaliseWriter(WFormat Format)
{
	TextFormat = Format;

	//copy over pallet
	BP_memcpy16(&BG_Pallet[239], TextFormat.PalletColours, 16);
}

void BP_InitaliseWriterStruct(unsigned short* inScreenBase,
							 unsigned short* inCharBase, 
							 unsigned short inMaxTiles, 
							 unsigned short* inPallet, 
							 unsigned short* inFont)
{
	TextFormat.ScreenBase = inScreenBase;
	TextFormat.CharBase	= inCharBase;
	TextFormat.MaxTiles	= inMaxTiles;
	TextFormat.PalletColours = inPallet;
	TextFormat.FontType = inFont;

	//copy over pallet
	BP_memcpy16(&BG_Pallet[239], TextFormat.PalletColours, 16);
}

void BP_Strcpy(char* dst, char* src)
{
	short i = 0;
	while(src[i] != '\0')
	{
		BP_memcpy8(&src[i], &dst[i], 1);
		i++;
	}
	dst[i] = '\0';
}

void BP_Strcat(char* dst, char* src)
{
	short i  = 0;
	short ii = BP_Strlen(dst);
	
	while(src[i] != '\0')
	{
		dst[ii++] = src[i++];
	}

}

short BP_Strlen(char* src)
{
	short i  = 0;
	while (src[i++] != '\0'){}
	return --i;
}

//stores how many characters have been read
unsigned char CharCount = 0;
//stores which tile print tiles should start at
unsigned short TileStart = 1;
//used to increase the character base
unsigned int charBaseLoc = 0;

int BP_PrintText(unsigned char* src, unsigned char x, unsigned char y)
{
	
	//stores the start and the end of the first loaded glyph and
	//the second loaded glyph
	unsigned int Glyph1End = 0;
	unsigned int Glyph2End = 0;
	//stores the current character in the string
	unsigned char CurrentChar = src[0];
	//a counter is created for the string
	unsigned short c = 0;
	//counter for the loops in the function 
	char i = 0;

	if (CurrentChar == '\n')
	{
		y+=2;
		CurrentChar = src[++c];
	}

	//if BP_SaveText is called then it will set rendered to true 
	//and the font will not be re-drawn until BP_ClearText is called
	if (!Rendered)
	{
		for (i = 0; i < 16; i++)
			CharBuffer[i] = unpack(TextFormat.FontType[(((CurrentChar - 32) * 16) + i)], &Glyph1End);
			//loops throught and unpacks the first character
		//the next character is then loaded
		CurrentChar = src[++c];

		while(src[c] != '\0')
		{
			if (CurrentChar == '\n')
			{

				//Tile is copied over into memory
				BP_memcpy32(&CharBuffer[0], TextFormat.CharBase + charBaseLoc, 16);
				charBaseLoc+= 0x020; //the amount the character base needs to be shifted is increased
				CharCount++; //The tile count is increased
				BP_PrintTiles(CharCount, TileStart, x, y);
				TileStart += CharCount << 1;
				CharCount = 0;
				y+=2;	
				CurrentChar = src[++c];
				Glyph1End = 0;
			}


			//Second Glyph is loaded
			for (i = 0; i < 16; i++)
				CharBuffer[i + 16] = unpack(TextFormat.FontType[(((CurrentChar - 32) * 16) + i)], &Glyph2End);
			//(3a) work out how much the glyph need to be shifted *4 as its in steps of 4 bits
			short gshift = Glyph1End << 2;

			if (Glyph1End != 0) //(3b) if the first glyth ended on 0 then no shift should be made
				for (i = 0; i < 16; i++) //(3c) if the charaer does need to be shifted then perform the shift
					CharBuffer[i] |= CharBuffer[i + 16] << gshift;
					//it is also orred as well to add them both together at the
					//same time as the shift
			if (Glyph1End == 0)
				for (i = 0; i < 16; i++) //(3c) if the charaer does need to be shifted then perform the shift
					CharBuffer[i] = CharBuffer[i + 16];

			//(4a) check is made to see if the glyph has taken up all the 8
			//pixels. if it has then it will need to be printed to the
			//character base
			if ((Glyph1End + Glyph2End) > 8)
			{
				//Tile is copied over into memory
				BP_memcpy32(&CharBuffer[0], TextFormat.CharBase + charBaseLoc, 16);
				charBaseLoc+= 0x020; //the amount the character base needs to be shifted is increased
				CharCount++; //The tile count is increased

				//The 2nd character now needs to be shifted back down to compensate for
				//the parts that have already been read
				gshift = (8 - Glyph1End) << 2;

				//(4b) the last rendered glyph is then set to
				//the first glyph
				for (i = 0; i < 16; i++)
					CharBuffer[i] = CharBuffer[i + 16] >> gshift;
				//if the glyph end was 0 then rest it to 8
				if (Glyph1End == 0)
					Glyph1End = 8;
				//work out where the next glyph end is for the first glyph
				Glyph1End = Glyph2End - (8 - Glyph1End);

			}
			else if ((Glyph1End + Glyph2End) == 8)
			{
				BP_memcpy32(&CharBuffer[0], TextFormat.CharBase + charBaseLoc, 16);
				charBaseLoc+= 0x020;
				CharCount++;

				CurrentChar = src[++c];
				if (CurrentChar == '\0')
					break;
				if (CurrentChar == '\n')
				{
					BP_PrintTiles(CharCount, TileStart, x, y);
					TileStart += CharCount << 1;
					CharCount = 0;
					y+=2;
					CurrentChar = src[++c];
				}

				for (i = 0; i < 16; i++)
					CharBuffer[i] = unpack(TextFormat.FontType[(((CurrentChar - 32) * 16) + i)], &Glyph1End);

			}
			else
			{
				//if a tile was not created from the extra glyph then the end
				//will have shifted with the size of that glyph
				Glyph1End += Glyph2End;
			}
			Glyph2End = 0;
			//increate the string
			CurrentChar = src[++c];
		}

		if (Glyph1End != 0)
		{
			BP_memcpy32(&CharBuffer[0], TextFormat.CharBase + charBaseLoc, 16);
			charBaseLoc+= 0x020;
			CharCount++;
		}
	}	

	BP_PrintTiles(CharCount, TileStart, x, y);
	TileStart += CharCount << 1;
	CharCount = 0;

	return CharCount;
}

void BP_PrintTiles(unsigned char count, unsigned short TileStart, unsigned char inX, unsigned char inY)
{
	unsigned short tile = TileStart; 
	char x;
	char y;
	for (x = 0; x < count; x++)
	{
		for (y = 0; y < 2; y++)
		{
			BP_RenderScreenBase(TextFormat.ScreenBase, inX + x, inY + y, tile, 1, 0, 0);
			tile++;
		}
	}
}

void BP_FinishWrite()
{
	Rendered = true;
}

void BP_ClearText()
{
	TileStart = 1;
	charBaseLoc = 0;
	Rendered = false;
	char x;
	char y;
	for (x = 0; x < 32; x++)
	{
		for (y = 0; y < 32; y++)
		{
			BP_RenderScreenBase(TextFormat.ScreenBase, x, y, 0, 1, 0, 0);
		}
	}
}

int unpack(unsigned short src,unsigned int* GlythEnd)
{
	unsigned int dst = 0; //output value
	char i = 0; //loop counter

	for(i = 0; i < 8; i++)
	{
		unsigned char tmp = 0;
		//read the first 2bits of the source
		tmp = (src & 0x03);
		if (tmp == 2)
		{
			*GlythEnd = i;
			break;
		}
		if (i == 7)
			*GlythEnd = 8;
		//shift the src down by 2bits as it they have just been read
		src = src >> 2;
		dst |= (tmp << ((i * 4)));
	}

	return dst;
}

