#include "G_PickUp.h"

void Pick_Initalise()
{
	BP_memcpy16(&PickUpPallet, &OBJPallet[80], 16);
	BP_memcpy32(&PH1, &OBJAdd32[1488], (PH1Tiles*8));//186
	BP_memcpy32(&PM1, &OBJAdd32[1496], (PM1Tiles*8));//187
	u8 i = 0;
	for (i = 0; i < 3; i++)
	{
		PickUps[i].Active = false;
		PickUps[i].Sprite = &OAM_Buffer[i + 123];
		BP_EnableOAM(PickUps[i].Sprite, false);
	}

	PickUpAnimation.startAdd = 0;
	PickUpAnimation.TileCount = 1;
	PickUpAnimation.Frames = 4;
	PickUpAnimation.FrameDuration = 6;

	PickUpTimer = rand() % 700 + 500;
	PickUpTimer += Frame_Count;

}

void Pick_Update()
{
	if ((Frame_Count - PickUpTimer) > 0 && (Frame_Count - PickUpTimer) < 20)
	{
		u8 i = 0;
		for (i = 0; i < 3; i++)
		{
			if(PickUps[i].Active == false)
			{
				PickUps[i].AnimationTime = Frame_Count;
				PickUps[i].TimeAlive = Frame_Count;
				PickUps[i].Active = true;
				u8 Type = rand() % 2 + 1;
				PickUps[i].Type = Type;
				if (Type == 2)
					BP_SetOAM(PickUps[i].Sprite, OAMATT0_Shape_Square, OAMATT1_Size_8, Set_ATTR2(187, 2, 5));
				else if(Type == 1)
					BP_SetOAM(PickUps[i].Sprite, OAMATT0_Shape_Square, OAMATT1_Size_8, Set_ATTR2(186, 2, 5));

				PickUps[i].Position.y = rand() % 130 + 10;
				PickUps[i].Position.x = rand() % 200 + 30;
				BP_SetOAMPos(PickUps[i].Sprite, PickUps[i].Position.y, PickUps[i].Position.x);
				BP_EnableOAM(PickUps[i].Sprite, true);
				break;
			}
			
		}

		PickUpTimer = rand() % 300 + 100;
		PickUpTimer += Frame_Count;
	}


	u8 i = 0;
	for (i = 0; i < 3; i++)
	{
		if(PickUps[i].Active == true)
		{
			switch(PickUps[i].Type)
			{
			case P_HEALTH:
					if(BP_RunAnimation(&PH1, PickUps[i].AnimationTime, &OBJAdd32[1488], &PickUpAnimation))
						PickUps[i].AnimationTime = Frame_Count;
					break;
			case P_DAMAGE:
					if(BP_RunAnimation(&PM1, PickUps[i].AnimationTime, &OBJAdd32[1496], &PickUpAnimation))
						PickUps[i].AnimationTime = Frame_Count;
					break;
			}


			if ((Frame_Count - PickUps[i].TimeAlive) >= 200)
			{
				PickUps[i].Active = false;
				BP_EnableOAM(PickUps[i].Sprite, false);
			}

		}

	}
}

PickType Pick_CheckCollision(Ccube* collisonBox)
{
	u8 i = 0;
	for (i = 0; i < 3; i++)
	{
		if(PickUps[i].Active == true)
		{
			PickCollisionBox.x = PickUps[i].Position.x;
			PickCollisionBox.y = PickUps[i].Position.y;
			PickCollisionBox.width	= 8;
			PickCollisionBox.height = 8;

			if (BP_CIntersectBox(&PickCollisionBox, collisonBox))
			{
				PickUps[i].Active = false;
				BP_EnableOAM(PickUps[i].Sprite, false);
				return PickUps[i].Type;
			}
		}
	}

	return 0;
}