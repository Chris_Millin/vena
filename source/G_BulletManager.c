#include "G_BulletManager.h"



void Bullets_Initalise()
{
	//set all bullets to be unavtive and at 0,0
	//==================================================================================
	u8 i = 0; 
	for (i = 0; i < MAX_BULLETS; i++)
	{
		Bullets[i].Active = false;
		Bullets->Position.x = 0;
		Bullets->Position.y = 0;
		Bullets[i].Position.x	= 0;
		Bullets[i].Position.y	= 0;
		Bullets[i].Damage		= 0;
		Bullets[i].Direction.x	= 0;
		Bullets[i].Direction.y	= 0;
		Bullets[i].FromPlayer	= false;
		BP_SetOAMPos(Bullets[i].Sprite, Bullets[i].Position.y, Bullets[i].Position.x);
		Bullets[i].Sprite		= 0;
		BP_SetOAMPos(Bullets[i].Sprite, Bullets[i].Position.y, Bullets[i].Position.x);
	}
	//==================================================================================
	//set the collision box of the player which is a bit smaller than him and has
	//an offset from his X and Y
	//==================================================================================
	BulletCBox.x = BULLET_X_OFFSET;
	BulletCBox.y = BULLET_X_OFFSET;
	BulletCBox.width	= BULLET_WIDTH;
	BulletCBox.height	= BULLET_HEIGHT;
	//==================================================================================


	//Loads the player sprite object and sets the position of the object
	//==================================================================================
	//Pallet0 will need to change
	BP_memcpy16(&Pallet1, &OBJPallet[16], 16); //Copy Over Bullet Pallet
	BP_memcpy16(&Pallet2, &OBJPallet[32], 16); //Copy Over Bullet Pallet
	BP_memcpy32(&NanoBullet, &OBJAdd32[OBJ_Offset], (NanoBulletTiles*8)); //Copy Over Bullet
	OBJ_Offset+= (NanoBulletTiles*8);
	BP_memcpy32(&EnemyBullet, &OBJAdd32[OBJ_Offset], (EnemyBulletTiles*8)); //Copy Over Bullet
	OBJ_Offset+= (EnemyBulletTiles*8);
	//==================================================================================
}

void Bullets_Update()
{
	u8 ActiveCount = 0;

	//update movement
	//==================================================================================
	u8 i = 0; 
	for (i = 0; i < MAX_BULLETS; i++)
	{
		if (Bullets[i].Active == true)
		{
			//yeah if your reading my source and wonder what this is well yeah there is no -x on the GBA so this is
			//a way of wraping it round the screen and also working out if the value overflowed as its a unsinged value
			//hope you had fun with your signed values and screen coordinates ;_;
			if (Bullets[i].Position.y == 0 | Bullets[i].Position.y > 65505)
				Bullets[i].Position.y = 256;
			if (Bullets[i].Position.y >= 240 & Bullets[i].Position.y <= 256)
			{
				Bullets[i].Active = false;
				Bullets[i].Position.y = 0;
				Bullets[i].Position.x = 0;
				BP_EnableOAM(Bullets[i].Sprite, false);
			}

			if (Bullets[i].Position.x <= 6)
				Bullets[i].Position.x = 256;
			if (Bullets[i].Position.x >= 240 & Bullets[i].Position.x <= 255)
			{
				Bullets[i].Position.y = 0;
				Bullets[i].Position.x = 0;
				Bullets[i].Active = false;
				BP_EnableOAM(Bullets[i].Sprite, false);
			}
			ActiveCount++;


			Bullets[i].Position.x += Bullets[i].Direction.x;
			Bullets[i].Position.y += Bullets[i].Direction.y;
			BP_SetOAMPos(Bullets[i].Sprite, Bullets[i].Position.y, Bullets[i].Position.x);

		}
	}
	//==================================================================================
}


void Bullets_Add(u16 PositionX, u16 PositionY , u16 Damage, u8 SpeedX, u8 SpeedY, bool FromPlayer)
{
	u8 i = 0; 
	for (i = 0; i < MAX_BULLETS; i++)
	{
		if (Bullets[i].Active == false)
		{
			
			Bullets[i].Position.x	= PositionX;
			Bullets[i].Position.y	= PositionY;
			Bullets[i].Damage		= Damage;
			Bullets[i].Direction.x	= SpeedX;
			Bullets[i].Direction.y	= SpeedY;
			Bullets[i].FromPlayer	= FromPlayer;
			Bullets[i].Active		= true;
			Bullets[i].Sprite		= &OAM_Buffer[i+30];
			if (FromPlayer)
				BP_SetOAM(Bullets[i].Sprite, OAMATT0_Shape_Square, OAMATT1_Size_8, Set_ATTR2(16, 2, 1));
			else
				BP_SetOAM(Bullets[i].Sprite, OAMATT0_Shape_Square, OAMATT1_Size_8, Set_ATTR2(17, 2, 2));
			
			BP_SetOAMPos(Bullets[i].Sprite, Bullets[i].Position.y, Bullets[i].Position.x);
			break;
		}
	}
}


u8 Bullet_checkCollision(Ccube* CollisionObj, bool Player)
{
	u8 MaxDamage = 0;
	u8 i = 0;
	for (i = 0; i < MAX_BULLETS; i++)
	{
		if (Bullets[i].Active == true && Bullets[i].FromPlayer != Player)
		{
			BulletCBox.x = Bullets[i].Position.x + BULLET_X_OFFSET;
			BulletCBox.y = Bullets[i].Position.y + BULLET_X_OFFSET;
			if(BP_CIntersectBox(CollisionObj, &BulletCBox))
			{
				Bullets[i].Position.y = 0;
				Bullets[i].Position.x = 0;
				Bullets[i].Active = false;
				BP_EnableOAM(Bullets[i].Sprite, false);
				MaxDamage += Bullets[i].Damage;
			}
		}
	}

	return MaxDamage;
}
