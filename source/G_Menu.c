#include "G_Menu.h"


void Menu_Initalise()
{

	//set up BG Modes
	// <summary>	Sets BG0 to use chaacter base block 31 for rendering. </summary>
	BGREG0 = (31 << 8) | (1 << 6) | 1;
	// <summary>	Sets BG0 to use chaacter base block 30 for rendering. </summary>
	BGREG1 = (30 << 8) | (1 << 6) | (1 << 2);

	KeyDown = false;
	CurrentState = M_ISTART;

	//Menu BG
	//==================================================================================
	MenuMap.ScreenBase	= SCREEN_BASE_BLOCK16(31);
	MenuMap.CharBase	= CHAR_BASE_BLOCK16(0);
	MenuMap.CurrentX	= 0;
	MenuMap.CurrentY	= 0;
	MenuMap.MapSrc		= (unsigned short*)b_MenuData;
	MenuMap.MapHeight	= 20;
	MenuMap.MapWidth	= 30;
	MenuMap.TileOffSet	= 0;

	BP_memcpy16(&sMenuPallet, &BG_Pallet[0], 16);
	BP_memcpy16(&b_MenuTiles, &MenuMap.CharBase[0], (b_MenuTileCount * 16));
	BP_RenderMap(MenuMap);
	//==================================================================================

	//Sprites
	//==================================================================================
	BP_memcpy16(&sMenuPallet, &OBJPallet[0], 16);
	BP_memcpy32(&Micro, &OBJAdd32[0], (MicroTiles*8));
	BP_memcpy32(&Logo, &OBJAdd32[(MicroTiles*8)], (LogoTiles*8));
	BP_memcpy32(&Cursor, &OBJAdd32[(MicroTiles*8) + (LogoTiles*8)], 8);

	mSprites[0] = &OAM_Buffer[0];
	mSprites[1] = &OAM_Buffer[1];
	mSprites[2] = &OAM_Buffer[2];
	//==================================================================================

	Menu_Update();
}

bool Menu_Update()
{
	BP_UpdateKeys();

	switch(CurrentState)
	{
	case M_ISTART:
		
		BGScrollAdd[1].x = 0;
		BG_Pallet[17] = BG_Pallet[15];
		BP_ClearText(); 
		BP_PrintText("A Ludum Dare 48hour Game\0", 6,0);
		BP_PrintText("Made By Chris Millin\0", 9,2);
		BP_PrintText("#LD24 27/08/2012\0", 9,4);
		BP_PrintText("Start\0", 6,11);
		BP_PrintText("Instructions\0", 11,11);
		BP_PrintText("About\0", 20,11);
		BP_FinishWrite();

		CursorFlicker = Frame_Count;



		BP_SetOAM(mSprites[0], OAMATT0_Shape_Square, OAMATT1_Size_64, Set_ATTR2(0, 0, 0));
		BP_SetOAMPos(mSprites[0], 100, 85);
		BP_SetOAM(mSprites[1], OAMATT0_Shape_Wide, OAMATT1_Size_64, Set_ATTR2(64, 0, 0));
		BP_SetOAMPos(mSprites[1], 50, 85);
		BP_SetOAM(mSprites[2], OAMATT0_Shape_Square, OAMATT1_Size_8, Set_ATTR2(96, 0, 0));
		BP_SetOAMPos(mSprites[2], 90, 76);
		

		MainSelection = 0;

		CurrentState = M_START;
		break;
	case M_START:
		if ((Frame_Count - CursorFlicker) <= 20)
			BP_EnableOAM(mSprites[2], true);
		if ((Frame_Count - CursorFlicker) >= 20 && (Frame_Count - CursorFlicker) < 40)
			BP_EnableOAM(mSprites[2], false);
		if ((Frame_Count - CursorFlicker) >= 40)
			CursorFlicker = Frame_Count;


		if (BP_isKeyPressed(Key_Left) &&  KeyDown == false)
		{
			if (MainSelection == 0)
			{
				MainSelection = 1;
			}
			MainSelection--;
			KeyDown = true;
		}
		if (BP_isKeyPressed(Key_Right) && KeyDown == false)
		{
			if (MainSelection == 2)
			{
				MainSelection = 1;
			}
			MainSelection++;
			KeyDown = true;
		}
		if (!BP_isKeyPressed(Key_Left) && !BP_isKeyPressed(Key_Right))
			KeyDown = false;

		if (MainSelection == 0)
		{
			BP_SetOAMPos(mSprites[2], 90, 76);
		}
		if (MainSelection == 1)
		{
			BP_SetOAMPos(mSprites[2], 90, 150);
		}
		if (MainSelection == 2)
		{
			BP_SetOAMPos(mSprites[2], 90, 190);
		}

		if (BP_isKeyPressed(Key_Start))
		{	
			if (MainSelection == 0)
			{
				return false;
			}
			if (MainSelection == 1)
			{
				CurrentState = M_IINST;
			}
			if (MainSelection == 2)
			{
				CurrentState = M_IABOUT;
			}
		}

		BP_OAMCpy(OAMAddress, OAM_Buffer, 3);
		break;
	case M_IINST:
		BP_EnableOAM(mSprites[0], false);
		BP_EnableOAM(mSprites[1], false);
		BP_EnableOAM(mSprites[2], false);
		BP_ClearText(); 
		BP_PrintText("Fight off the evolving virus as a nano machine.\nThe virus will build and become more\nferocious the more you kill untill you\nwipe them out \0", 0,1);
		BP_PrintText("Keys: [A] - Fire [DPAD] - Move \0", 0,10);
		BP_PrintText("Dont Forget to collect the Red |H|\nand blue |X| \0", 0,12);
		BP_PrintText("Exit [B]", 0, 18);
		BP_FinishWrite();

		BGScrollAdd[1].x = 250;

		CurrentState = M_INST;
		break;
	case M_INST:
		BP_EnableOAM(mSprites[0], false);
		BP_EnableOAM(mSprites[1], false);
		BP_EnableOAM(mSprites[2], false);

		if (BP_isKeyPressed(Key_B))
		{	
			CurrentState = M_ISTART;
		}
		BP_OAMCpy(OAMAddress, OAM_Buffer, 3);
		break;
	case M_IABOUT:
		BP_EnableOAM(mSprites[0], false);
		BP_EnableOAM(mSprites[1], false);
		BP_EnableOAM(mSprites[2], false);
		BP_ClearText(); 
		BP_PrintText("Blog: https://kotadev.wordpress.com/\0", 0,1);
		BP_PrintText("Website: http://www.chrismillin.co.uk/\0", 0,3);
		BP_PrintText("Shout Out To:\nSi Jordan, Joe Thompson,\nMatrin, Tim Dykes,\nAndrew Martin, Neil Dansey &\nLittle Kraken Games Studio\0", 0,6);
		BP_PrintText("Exit [B]", 0, 18);
		BP_FinishWrite();

		BGScrollAdd[1].x = 250;

		CurrentState = M_ABOUT;
		break;
	case M_ABOUT:
		if (BP_isKeyPressed(Key_B))
		{	
			CurrentState = M_ISTART;
		}
		BP_OAMCpy(OAMAddress, OAM_Buffer, 3);
		break;
	}

	BP_VBlankWait();

	return true;
}
