




    .global NoCash_Print    @int NoCash_Print(const char* str);
	.global NoCash_Msg		@EWRAM_CODE void NoCash_Msg(void);
	.global NoCash_Buffer	@extern EWRAM_DATA char NoCash_Buffer[80];


    .text               @ Put the following stuff in the "text" (== code) section
    .align 2            @ align to 1<<2 bytes
    .thumb_func         @ Call as thumb function
	.global NoCash_Print
NoCash_Print:                
    @ start of function
	push	{r4, lr}			@pushes r4 and the Link Register onto the stack
	ldr		r4, =NoCash_Msg		@Stores the address for the NoCash_Msg function
	ldr		r1, =NoCash_Buffer	@Stores the address of the buffer

	mov		r2, #0				@initalise r2 to 0
	mov		r12, r2				@mover the value of r2 into r12 as well
	
.Lmsg_loop:
		mov	r2, #0

.Lmsg_cpy:
			ldrb	r3, [r0, r2]	@loads the data from r0 (per byte) and stores it in r3
			strb	r3, [r1, r2]	@puts the data sent to the function into the buffer
			cmp		r3, #0			@check to see if r3 == '\0' in which case it has hit
									@the end of the string
			beq		.Lmsg_print		@break to print the mesage
			add		r2, #1			@increase the loop by 1
			cmp		r2, #80			@if the loop hits 80 then it needs to break as there is
									@only space for 80 character and we would buffer overflow if
									@any more data was copied
			bne		.Lmsg_cpy		@if it has not reached 80 character then loop

.Lmsg_print:
		bl		.Lmsg_out

		@if '\0' was not hit then we need to carry on the message
		add		r0, r2				@this should increase the string by 80 characters
		add		r12, r2				@increases r12 by 80 as well (this is used to print the message)
		cmp		r3, #0				@check to see if '\0' was hit just incase
		bne		.Lmsg_loop

	@this will output the int value though I have no idea
	@why r1 is poped maybe its from the stack which will be the int data
	mov		r0, r12
	pop		{r4}
	pop		{r1}
	bx		r1

.Lmsg_out:
	bx		r4




.section .ewram , "ax", %progbits
.thumb_func	
.align 2;								
.global NoCash_Msg

NoCash_Msg:
	mov		r12, r12
	b		.Lmsg_end
	.hword 0x6464
	.hword 0

NoCash_Buffer:
	.space	82

.Lmsg_end:
    bx lr

@ end of function 